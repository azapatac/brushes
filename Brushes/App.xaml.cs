﻿using Brushes.Views.Pages;
using Xamarin.Forms;

namespace Brushes
{
    public partial class App : Application
    {
        public App()
        {
            Device.SetFlags(new[] { "Brush_Experimental", "Shapes_Experimental" });
            InitializeComponent();
            MainPage = new BrushesPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
